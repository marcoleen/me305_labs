# Marco Leen, ME 305 Lab 0x00 

def fib (idx):                              # Function for solving Fibonacci numbers using a bottom-up approach
    if idx == 0:                            # Return values for index 0, 1
        return 0
    elif idx == 1:
        return 1
    elif idx > 1:                           # Calculating for values greater than 1
        i = 1                               # counter
        answer = 0                          # return value
        fn1 = 0                             # stored value 1
        fn2 = 1                             # stored value 2
        while i <= idx:                     # while loop to count up to input index
            fn1 = fn2                       # replace values with stored answer and shifts over before resetting answer until counter is index
            fn2 = answer
            answer = fn1 + fn2
            i += 1
        else:
            return answer
    else:
        pass  

def interface():                           # User interface function. Recieves entry and from main, sends entry to fib(idx), prints return value from fib(idx)
    entry = input('Choose an index:')
    try:
        idx = int(entry)
    except:
        print('Index must be an integer.')
        interface()
    else:
        if idx < 0:
            print('Index must be nonnegative.')
            interface()
        elif idx == 0:
            print('Fibonacci number at index 0 is 0.')
        elif idx == 1:
            print('Fibonacci number at index 1 is 1.')
        else:
            fib(idx)
            print('Fibonacci number at index {:} is {:}.'.format(idx,fib(idx)))
if __name__ == '__main__':
    det = 0
    while True:                            # While true loop that prompts user with requests for inputs and sends to interface() until broken by user
        if det == 0:
            interface()
            det += 1
        redo = input('Enter y to try another index. Enter q to quit.')
        if redo == 'y':
            interface()
        elif redo == 'q':
            print('Maybe later.')
            break
        else:
            print('Please enter y or q only.')